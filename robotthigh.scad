$fa = 3;
$fs = 1;

thick = 4.1;
fudge = 0.1;

hthick = 1.6;
longarm = 18.3;
shortarm = 16.29;
hlength = 34.3;
hwidth = 17.2;
shortendrad = 4.1 / 2;
longendrad = 4.3 / 2;
longrad = 7.4 / 2;
shortrad = 6 / 2;
screwrad = 5.5 / 2;
screwholerad = 3.5 / 2;
chamferrad = 8.5 / 2;
bossrad = 7.3 / 2;

servolength = 22.68;
servowidth = 22.85;
servothick = 12.6;
servoflangelength = 4.9;
servoflangethick = 2.5;
servoflangeoffset = 4.3;
servoscrewrad = 1.8 / 2;
screwslotwidth = 1;
screwslotlength = 3;

bevel = 8;
bevellen = bevel * 1.42;

holewide = 3;
holelong = 9;
holeoffset = 2;
holeback = 8;

offset = 25;

length = hlength / 2 + bossrad * 2 + servowidth + 2 * servoflangelength + offset - 5;
widthrad = servolength / 2;

rightmeat = 1.5;

difference() {
    union() {
        hull() {
            cylinder(h = thick, r = widthrad);
            translate(v = [0, length, 0]) {
                cylinder(h = thick, r = widthrad);
            }
        }

        translate(v = [-widthrad / 2, (hlength / 2) + offset - rightmeat, thick - fudge]) {
            difference() {
                cube(size = [servothick, servoflangelength + rightmeat, servothick]);
                translate(v = [-fudge, rightmeat + servoflangelength / 2, servothick / 2]) {
                    translate(v = [0, -screwslotwidth/2, -screwslotlength/2]) {
                        cube(size = [2*servothick, screwslotwidth, screwslotlength]);
                    }
                    rotate(a = [0, 90, 0]) {
                        cylinder(h = servothick + 2 * fudge, r = servoscrewrad);
                    }
                }
                translate(v = [0, -fudge, servothick - bevel]) {
                    rotate(a = [0, -45, 0]) {
                        cube(size = [bevellen, rightmeat + servoflangelength + 2 * fudge, bevellen]);
                    }
                }
                    translate(v = [-fudge, -fudge, 4]) {
                        cube(size = [2.8 + fudge, 3 + servoflangelength + 2 * fudge, 6]);
                    }
            }
        }
        translate(v = [-widthrad / 2, (hlength / 2) + offset + servowidth + servoflangelength, thick - fudge]) {
            difference() {
                cube(size = [servothick, 3 + servoflangelength, servothick]);
                translate(v = [-fudge, servoflangelength / 2, servothick / 2]) {
                    translate(v = [0, -screwslotwidth/2, -screwslotlength/2]) {
                        cube(size = [2*servothick, screwslotwidth, screwslotlength]);
                    }
                    rotate(a = [0, 90, 0]) {
                        cylinder(h = servothick + 2 * fudge, r = servoscrewrad);
                    }
                }
                translate(v = [0, -fudge, servothick - bevel]) {
                    rotate(a = [0, -45, 0]) {
                        cube(size = [bevellen, 3 + servoflangelength + 2 * fudge, bevellen]);
                    }
                }
            }
        }
    }
    
    // wire hole
    translate(v = [-holeback, hlength / 2 + bossrad * 2 + holeoffset, -fudge]) {
        //cylinder(h = thick + 2 * fudge, r = holerad);
        cube(size = [holewide, holelong, thick + 2 * fudge]);
    }
    
    // servo horn cutout
    translate(v = [0, 9, thick + fudge - hthick]) {
        union() {
            hull() {
                translate(v = [0, longarm - longendrad, 0]) {
                    cylinder(h = hthick + fudge, r = longendrad);
                }
                cylinder(h = hthick, r = longrad);
            }
            hull() {
                translate(v = [0, longendrad - shortarm, 0]) {
                    cylinder(h = hthick + fudge, r = longendrad);
                }
                cylinder(h = hthick, r = shortrad);
            }
            hull() {
                translate(v = [hwidth / 2 - shortendrad, 0, 0]) {
                    cylinder(h = hthick + fudge, r = shortendrad);
                }
                translate(v = [-(hwidth / 2 - shortendrad), 0, 0]) {
                    cylinder(h = hthick + fudge, r = shortendrad);
                }
            }
            cylinder(h = thick + fudge, r = chamferrad);
            translate(v = [0, 0, hthick - (thick + 2 * fudge)]) {
                cylinder(h = thick + 2 * fudge, r = bossrad);
            }
        }
    }
}