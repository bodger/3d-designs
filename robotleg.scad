$fa = 3;
$fs = 1;

outerrad = 62;
innerrad = 52;
offset = -13;
thick = 4.1;
fudge = 0.1;

hthick = 1.6;
longarm = 18.24;
shortarm = 16.29;
hlength = 34.3;
hwidth = 17.1;
shortendrad = 4 / 2;
longendrad = 4.3 / 2;
longrad = 7.3 / 2;
shortrad = 6 / 2;
screwrad = 5.5 / 2;
screwholerad = 3.5 / 2;
chamferrad = 8.1 / 2;
bossrad = 7.3 / 2;

difference() {
    union() {
        difference() {
            cylinder(h = thick, r = outerrad);
            translate(v = [offset, 0, -fudge]) {
                cylinder(h = thick + 2 * fudge, r = innerrad);
            }
            translate(v = [-outerrad, 0, -fudge]) {
                cube(size = [outerrad * 2, outerrad, thick + 2 * fudge]);
            }
            translate(v = [-62, -49, -fudge]) {
                rotate(a = [0, 0, -18]) {
                    cube(size = [36, 19, thick + 2 * fudge]);
                }
            }
        }
        translate(v = [(outerrad + innerrad + offset)/2, 0, 0]) {
            cylinder(h = thick, r = (-offset + outerrad - innerrad) / 2);
        }
        translate(v = [-25.2, -53.47, 0]) {
            cylinder(h = thick, r = 2.87);
        }
    }
    
    translate(v = [(outerrad + innerrad + offset)/2 - 1, -10, thick - hthick]) {
        rotate(a = [0, 0, -9]) {
            union() {
                hull() {
                    translate(v = [0, shortarm - longendrad, 0]) {
                        cylinder(h = hthick + fudge, r = longendrad);
                    }
                    cylinder(h = hthick, r = longrad);
                }
                hull() {
                    translate(v = [0, longendrad - longarm, 0]) {
                        cylinder(h = hthick + fudge, r = longendrad);
                    }
                    cylinder(h = hthick, r = shortrad);
                }
                hull() {
                    translate(v = [hwidth / 2 - shortendrad, 0, 0]) {
                        cylinder(h = hthick + fudge, r = shortendrad);
                    }
                    translate(v = [-(hwidth / 2 - shortendrad), 0, 0]) {
                        cylinder(h = hthick + fudge, r = shortendrad);
                    }
                }
                cylinder(h = hthick + fudge, r = chamferrad);

                translate(v = [0, 0, -(thick + fudge - hthick)]) {
                    cylinder(h = thick + 2 * fudge, r = bossrad);
                }
            }
        }
    }
}