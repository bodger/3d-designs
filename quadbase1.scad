$fa = 3;
$fs = 1;

inch = 25.4;
mil = inch / 1000;

width = 110;
height = 110;

thick = 3.5;
fudge = 0.1;
radius = 8;

servolength = 22.68;
servowidth = 22.85;
servothick = 12.6;
servoflangelength = 4.9;
servoflangethick = 2.5;
servoflangeoffset = 4.3;
servoscrewrad = 1.8 / 2;
screwslotwidth = 1;
screwslotlength = 3;

arduinox = 5;
arduinoy = 3;
arduinohole = 3.25 / 2;

servobx = 37;
servoby = 50;
servobhole = 2.5 / 2;

fthick = thick + 2 * fudge;

module servo() {
  cube(size = [servowidth, servothick, fthick], center = true);
  translate(v = [(servowidth + servoflangelength) / 2, 0, 0]) {
      cylinder(h = fthick, r = servoscrewrad, center = true);
      rotate(a = [90, 90, 0]) {
          translate(v = [-fthick, -screwslotwidth/2, -screwslotlength/2]) {
              cube(size = [fthick + 2, screwslotwidth, screwslotlength]);
          }
      }
  }
  translate(v = [-(servowidth + servoflangelength) / 2, 0, 0]) {
      cylinder(h = fthick, r = servoscrewrad, center = true);
      rotate(a = [90, 90, 0]) {
          translate(v = [-fthick, -screwslotwidth/2, -screwslotlength/2]) {
              cube(size = [fthick + 2, screwslotwidth, screwslotlength]);
          }
      }
  }
}

difference() {
    hull() {
        translate(v = [radius, radius, 0]) {
            cylinder(h = thick, r = radius);
        }
        translate(v = [width - radius, radius, 0]) {
            cylinder(h = thick, r = radius);
        }
        translate(v = [radius, height - radius, 0]) {
            cylinder(h = thick, r = radius);
        }
        translate(v = [width - radius, height - radius, 0]) {
            cylinder(h = thick, r = radius);
        }
    }
    // Arduino
    translate(v = [arduinox, arduinoy, -fudge]) {
        translate(v = [600 * mil, 2000 * mil, 0]) {
            cylinder(h = fthick, r = arduinohole);
        }
        translate(v = [2600 * mil, 1400 * mil, 0]) {
            cylinder(h = fthick, r = arduinohole);
        }
        translate(v = [2600 * mil, 300 * mil, 0]) {
            cylinder(h = fthick, r = arduinohole);
        }
    }
    // servo board
    translate(v = [servobx, servoby, -fudge]) {
        rotate(0) {
        translate(v = [0.13 * inch, 0.13 * inch, 0]) {
            cylinder(h = fthick, r = servobhole);
        }
        translate(v = [0.13 * inch, 0.88 * inch, 0]) {
            cylinder(h = fthick, r = servobhole);
        }
        translate(v = [2.33 * inch, 0.13 * inch, 0]) {
            cylinder(h = fthick, r = servobhole);
        }
        translate(v = [2.33 * inch, 0.88 * inch, 0]) {
            cylinder(h = fthick, r = servobhole);
        }
      }
    }
    // servo
    translate(v = [17, 17, thick / 2]) {
        rotate(a = [0, 0, 45]) {
            servo();
        }
    }
    translate(v = [width - 17, 17, thick / 2]) {
        rotate(a = [0, 0, -45]) {
            servo();
        }
    }
    translate(v = [width - 17, height - 17, thick / 2]) {
        rotate(a = [0, 0, 45]) {
            servo();
        }
    }
    translate(v = [17, height - 17, thick / 2]) {
        rotate(a = [0, 0, -45]) {
            servo();
        }
    }
}