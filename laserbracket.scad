
module laserheadplate()
{
	length       = 80;
	width        = 50;
	height       = 60;
	bracket      = 40;
	spacing      = 18;
	thickness    = 3;
	inset        = 10;
	bracketbevel = 16;
	brackethole  = 3.2;
    bracketholerad = brackethole / 2;
	headhole     = 5.0;
    headholerad  = headhole / 2;
    headslot     = 8.0;
	brackettop   = bracket - thickness;
	gusset       = (brackettop - inset) * sqrt(2);
	//gussetpos    = (width - spacing) / 2;
    gussetpos    = 20;
	fudge        = 1;
	top          = height + fudge - thickness;
    $fs          = 0.25;

	translate(v = [-length / 2, -width / 2, 0]) {
		difference() {
			// original brick
			cube(size = [length, width, height], center = false);
	
			// remove material under bracket
			translate (v = [bracket, -fudge, thickness]) {
				cube(size = [length + fudge - bracket,
					width + 2 * fudge,
					top]);
			}
	
			// remove material between bracket gussets
			//translate (v = [-fudge, gussetpos, thickness]) {
			//	cube(size = [brackettop + fudge,
			//		spacing,
			//		top]);
			//}
            
            translate (v = [-fudge, thickness, thickness]) {
                cube(size = [brackettop + fudge,
                    gussetpos - thickness * 1.5, top]);
            }
	
            translate (v = [-fudge, gussetpos + thickness / 2, thickness]) {
                cube(size = [brackettop + fudge,
                    width - (gussetpos + thickness * 1.5), top]);
            }
	
			// remove material from outer sides of gussets
			//translate (v = [-fudge, -fudge, thickness]) {
			//	cube(size = [brackettop + fudge,
			//		gussetpos + fudge - thickness,
			//		top]);
			//}
	
			//translate (v = [-fudge, width + thickness - gussetpos, thickness]) {
			//	cube(size = [brackettop + fudge,
			//		gussetpos + fudge - thickness,
			//		top]);
			//}
	
			// remove material from beyond gussets
			translate (v = [-fudge, -fudge, thickness]) {
				cube(size = [inset + fudge, width + 2 * fudge, top]);
			}
	
			translate (v = [-fudge, -fudge, thickness + brackettop - inset]) {
				cube (size = [brackettop + fudge,
					width + 2 * fudge,
					height + fudge]);
			}
	
			// bevel gussets
			translate (v = [inset, -fudge, thickness]) {
				rotate (a = [0, -45, 0]) {
					cube(size = [gusset, width + 2 * fudge, gusset]);
				}
			}
	
			// bevel bracket
			translate (v = [brackettop - fudge, 0, height - bracketbevel]) {
				rotate (a = [45, 0, 0]) {
					cube(size = [thickness + 2 * fudge,
						2 * bracketbevel,
						2 * bracketbevel]);
				}
			}
	
			translate (v = [brackettop - fudge, width, height - bracketbevel]) {
				rotate (a = [45, 0, 0]) {
					cube(size = [thickness + 2 * fudge,
						2 * bracketbevel,
						2 * bracketbevel]);
				}
			}
	
			// bracket holes
	
			translate (v = [bracket + fudge, 10, thickness + 8.5]) {
				rotate (a = [0, -90, 0]) {
                    cylinder(h = thickness + 2 * fudge, r = bracketholerad);
                }
			}
	
			translate (v = [bracket + fudge, 41.5, thickness + 8.5]) {
				rotate (a = [0, -90, 0]) {
					cylinder(h = thickness + 2 * fudge, r = bracketholerad);
				}
			}
	
			translate (v = [bracket + fudge, width - 17, thickness + 51]) {
				rotate (a = [0, -90, 0]) {
					cylinder(h = thickness + 2 * fudge, r = bracketholerad);
				}
			}
	
			// laser head mounting holes
	
			translate (v = [25, 31, -fudge]) {
                hull() {
                    cylinder(h = thickness + 2 * fudge, r = headholerad);
                    translate(v = [headslot, 0, 0]) {
                        cylinder(h = thickness + 2 * fudge, r = headholerad);
                    }
                }
			}

			//translate (v = [11, 21, -fudge]) {
            //    hull() {
            //        cylinder(h = thickness + 2 * fudge, r = headhole);
            //        translate(v = [headslot, 0, 0]) {
            //            cylinder(h = thickness + 2 * fudge, r = headhole);
            //        }
            //    }
			//}
	
			translate (v = [50, 31, -fudge]) {
                hull() {
				    cylinder(h = thickness + 2 * fudge, r = headholerad);
                    translate(v = [headslot, 0, 0]) {
                        cylinder(h = thickness + 2 * fudge, r = headholerad);
                    }
                }
			}
		}
	}
}

laserheadplate();

