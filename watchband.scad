$fa = 3;
$fs = 1;

width     = 25;
dia       = 42;
straight  = 40;
thickness =  3;
groove    =  1;
bottom    =  2;
fudge     =  0.1;
circuitthick  = 6;
circuitmargin = 4;
circuitwall   = 2;
circuitgroove = 10;
circuitbevel  = 3;
screwdia      = 1;
screwbottom   = 1;
screwboss     = 2.4;
displaythick  = 3.5;
displaymargin = 1;
displaywall   = 2;
displaygroove = 8;

radius      = dia / 2;
screwradius = screwdia / 2;
circuitx    = straight - (2 * (circuitmargin + circuitwall));
circuity    = width - 2 * circuitwall;
displayx    = straight - (2 * (displaymargin + displaywall));
displayy    = width - 2 * displaywall;

difference() {
    union() {
        difference() {
            // outer oval
            hull() {
                translate(v = [straight / 2, 0, 0]) {
                    cylinder(h = width, r = radius + thickness);
                }
                translate(v = [-straight / 2, 0, 0]) {
                    cylinder(h = width, r = radius + thickness);
                }
            }
            // inner oval
            hull() {
                translate(v = [straight / 2, 0, -fudge]) {
                    cylinder(h = width + 2 * fudge, r = radius);
                }
                translate(v = [-straight / 2, 0, -fudge]) {
                    cylinder(h = width + 2 * fudge, r = radius);
                }
            }
        }
        // circuitry box
        translate(v = [-straight / 2 + circuitmargin, radius, 0]) {
            cube(size = [straight - 2*circuitmargin, circuitthick, width]);
        }
        // display box
        rotate(a = [0, 0, 180]) {
            translate(v = [-straight / 2 + displaymargin, radius, 0]) {
                cube(size = [straight - 2 * displaymargin, displaythick, width]);
            }
        }
    }

    // curved groove
    translate(v = [straight / 2, 0, bottom]) {
        difference() {
            cylinder(h = width, r = radius + thickness / 2 + groove / 2);
            cylinder(h = width, r = radius + thickness / 2 - groove / 2);
            translate(v = [-dia, -(radius + thickness), -fudge]) {
                cube(size = [dia, dia + 2 * thickness, width + 2 * fudge]);
            }
        }
    }

    // groove extension to circuitry box
    translate(v = [straight / 2 + fudge - circuitgroove, radius + (thickness - groove) / 2, bottom]) {
        cube(size = [circuitgroove + fudge, groove, width]);
    }

    // grove extension to display box
    translate(v = [straight / 2 + fudge - displaygroove, -(radius + thickness - groove), bottom]) {
        cube(size = [displaygroove + fudge, groove, width]);
    }

    // opening for circuitry
    translate(v = [-straight / 2 + circuitmargin + circuitwall, radius + thickness / 2, circuitwall]) {
        difference() {
            cube(size = [straight - (2 * (circuitmargin + circuitwall)), circuitthick, width - 2 * circuitwall]);
            // corner bevels
            translate(v = [0, 0, 0]) {
                rotate(a = [0, 45, 0]) {
                    cube(size = [circuitbevel, 2 * circuitthick + 2, circuitbevel], center = true);
                }
            }
            translate(v = [circuitx, 0, 0]) {
                rotate(a = [0, 45, 0]) {
                    cube(size = [circuitbevel, 2 * circuitthick + 2, circuitbevel], center = true);
                }
            }
            translate(v = [circuitx, 0, circuity]) {
                rotate(a = [0, 45, 0]) {
                    cube(size = [circuitbevel, 2 * circuitthick + 2, circuitbevel], center = true);
                }
            }
            translate(v = [0, 0, circuity]) {
                rotate(a = [0, 45, 0]) {
                    cube(size = [circuitbevel, 2 * circuitthick + 2, circuitbevel], center = true);
                }
            }
        }
        // circuitry screw holes
        translate(v = [0, screwbottom, 0]) {
            rotate(a = [-90, 0, 0]) {
                cylinder(h = circuitthick, r = screwradius);
            }
        }
        translate(v = [circuitx, screwbottom, 0]) {
            rotate(a = [-90, 0, 0]) {
                cylinder(h = circuitthick, r = screwradius);
            }
        }
        translate(v = [circuitx, screwbottom, circuity]) {
            rotate(a = [-90, 0, 0]) {
                cylinder(h = circuitthick, r = screwradius);
            }
        }
        translate(v = [0, screwbottom, circuity]) {
            rotate(a = [-90, 0, 0]) {
                cylinder(h = circuitthick, r = screwradius);
            }
        }
    }
    // opening for display
    rotate(a = [0, 0, 180]) {
        translate(v = [-straight / 2 + displaymargin + displaywall, radius + thickness / 2 - groove / 2, displaywall]) {
            difference() {
                cube(size = [straight - (2 * (displaymargin + displaywall)), displaythick, width - 2 * displaywall]);
                // corner bosses
                translate(v = [0, 0, 0]) {
                    rotate(a = [-90, 0, 0]) {
                        cylinder(h = displaythick, r = screwboss);
                    }
                }
                translate(v = [displayx, 0, 0]) {
                    rotate(a = [-90, 0, 0]) {
                        cylinder(h = displaythick, r = screwboss);
                    }
                }
                translate(v = [displayx, 0, displayy]) {
                    rotate(a = [-90, 0, 0]) {
                        cylinder(h = displaythick, r = screwboss);
                    }
                }
                translate(v = [0, 0, displayy]) {
                    rotate(a = [-90, 0, 0]) {
                        cylinder(h = displaythick, r = screwboss);
                    }
                }
            }
            // display screw holes
            translate(v = [0, screwbottom, 0]) {
                rotate(a = [-90, 0, 0]) {
                    cylinder(h = displaythick, r = screwradius);
                }
            }
            translate(v = [displayx, screwbottom, 0]) {
                rotate(a = [-90, 0, 0]) {
                    cylinder(h = displaythick, r = screwradius);
                }
            }
            translate(v = [displayx, screwbottom, displayy]) {
                rotate(a = [-90, 0, 0]) {
                    cylinder(h = displaythick, r = screwradius);
                }
            }
            translate(v = [0, screwbottom, displayy]) {
                rotate(a = [-90, 0, 0]) {
                    cylinder(h = displaythick, r = screwradius);
                }
            } 
        }
    }
}