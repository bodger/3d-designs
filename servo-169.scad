servolength = 22.68;
servowidth = 22.85;
servothick = 12.6;
servoflangelength = 4.9;
servoflangethick = 2.5;
servoflangeoffset = 4.3;
servoscrewrad = 1.8 / 2;

module servo(thick, fudge) {
  fthick = thick + 2 * fudge;

  translate(v = [0, 0, thick / 2]) {
    cube(size = [servowidth, servothick, fthick], center = true);

    translate(v = [(servowidth + servoflangelength) / 2, 0, 0]) {
      cylinder(h = fthick, r = servoscrewrad, center = true);
    }

    translate(v = [-(servowidth + servoflangelength) / 2, 0, 0]) {
      cylinder(h = fthick, r = servoscrewrad, center = true);
    }
  }
}
