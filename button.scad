module button()
{
    buttondia    = 70;
    buttonthick  =  2;
    holedia      =  2;
    holespacing  =  5;

    buttonradius = buttondia   / 2;
    holeradius   = holedia     / 2;
    holeoffset   = holespacing / 2;

    difference() {
      cylinder(h = buttonthick, r1 = buttonradius, r2 = buttonradius, center = false);

     translate(v = [holeoffset, holeoffset, -1]) {
       cylinder(h = buttonthick + 2, r1 = holeradius, r2 = holeradius);
     } 
     translate(v = [holeoffset, -holeoffset, -1]) {
       cylinder(h = buttonthick + 2, r1 = holeradius, r2 = holeradius);
     } 
     translate(v = [-holeoffset, holeoffset, -1]) {
       cylinder(h = buttonthick + 2, r1 = holeradius, r2 = holeradius);
     } 
     translate(v = [-holeoffset, -holeoffset, -1]) {
       cylinder(h = buttonthick + 2, r1 = holeradius, r2 = holeradius);
     } 
    }
}

button();
