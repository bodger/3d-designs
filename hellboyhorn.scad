module horn()
{
    $fa          = 3;
    $fs          = 1;
    topdia       = 56;
    bottomdia    = 62;
    thick        = 25;

    headdepth    = 3.3;
    headdia      = 44;
    curvefrac    = 1.3;

    headradius   = ((headdepth * headdepth) + ((headdia * headdia) / 4)) / (2 * headdepth);
    
    

    difference() {
      cylinder(h = thick + headdepth, r1 = topdia / 2, r2 = bottomdia / 2, center = false);
      translate(v = [0, 0, thick + headradius - headdepth]) {
        scale([1.0, curvefrac, 1.0]) {
          sphere(r = headradius);
        }
      }
      translate(v = [-5, 15, 1]) {
          sphere(r=2);
      }
      translate(v = [-12, 16, 0]) {
          sphere(r=1);
      }
      translate(v = [6, 11, 0]) {
          sphere(r=1);
      }
      translate(v = [9, 10, 0]) {
          sphere(r=1);
      }
      translate(v = [3, 25, 0]) {
          sphere(r=1);
      }
      translate(v = [-24, 6, 0]) {
          sphere(r=1);
      }
      translate(v = [19, 13, 0]) {
          sphere(r=1);
      }
      translate(v = [8, -3, 0]) {
          sphere(r=1);
      }
      translate(v = [11, -7, 0]) {
          sphere(r=2);
      }
      translate(v = [-4, 5, 0]) {
          sphere(r=1);
      }
      translate(v = [-8, 4, 0]) {
          sphere(r=1);
      }
      translate(v = [-17, -5, 0]) {
          sphere(r=1);
      }
      translate(v = [-18, -9, 0]) {
          sphere(r=1);
      }
      translate(v = [5, -9, 0]) {
          sphere(r=1);
      }
      translate(v = [6, -14, 0]) {
          sphere(r=1);
      }
      translate(v = [-3, -15, 0]) {
          sphere(r=1);
      }
      translate(v = [-4, -24, 0]) {
          sphere(r=1);
      }
      translate(v = [11, -16, 0]) {
          sphere(r=1);
      }

      rotate(a = [0, 90, 0]) {
        translate(v = [0, 3, 4]) {
          cylinder(h = 25, r1 = .7, r2 = 1);
          sphere(r = .7);
        }
        translate(v = [0, -2, -22]) {
          cylinder(h = 62, r1 = .2, r2 = 1);
        }
        translate(v = [0, -14, -7]) {
          sphere(r = 1);
        }
        translate(v = [0, 18, -28]) {
            cylinder(h = 71, r1 = .1, r2 = 1);
        }
        translate(v = [0, 8, -11]) {
            sphere(r = 1);
        }
        translate(v = [0, -21, -19]) {
            cylinder(h = 32, r1 = 1, r2 = .3);
            *sphere(r = .3);
        }
        translate(v = [0, -11, -4]) {
            cylinder(h = 30, r1 = 1, r2 = 1);
            sphere(r = 1);
        }
      }
      hull() {
        rotate(a = [90, 90, 0]) {
            translate(v = [-19, -5, -40]) {
                cylinder(h = 80, r1 = 2, r2 = 2);
            }
            translate(v = [-19, 5, -40]) {
                cylinder(h = 80, r1 = 2, r2 = 2);
            }
        }
     }   
    }
}

horn();

