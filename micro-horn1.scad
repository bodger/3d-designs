$fa = 3;
$fs = 1;

hthick = 1.5;
length = 27.17;
width = 15.71;
endrad = 3.9 / 2;
centrad = 4.77 / 2;

union() {
    hull() {
        translate(v = [length / 2 - endrad, 0, 0]) {
            cylinder(h = hthick, r = endrad);
        }
        cylinder(h = thick, r = centrad);
        translate(v = [-(length / 2 - endrad), 0, 0]) {
            cylinder(h = hthick, r = endrad);
        }
    }
    hull() {
        translate(v = [0, width / 2 - endrad, 0]) {
            cylinder(h = hthick, r = endrad);
        }
        translate(v = [0, -(width / 2 - endrad), 0]) {
            cylinder(h = hthick, r = endrad);
        }
    }
}