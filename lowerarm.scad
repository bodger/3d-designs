include <constants.scad>;

thick = 4.1;

servolength = 22.68;
servowidth = 22.85;
servothick = 12.6;
servoflangelength = 4.9;
servoflangethick = 2.5;
servoflangeoffset = 4.3;
servoscrewrad = 1.8 / 2;
servoscrewlen = 10;
screwslotheight = 3;
screwslotwidth = 1;
screwslotdepth = 9;

bevel = 8;
bevellen = bevel * 1.42;

liftservopos = 82;
elbowservopos = 9;
hingepos = 53;
hingerad = 3;
hingeheight = 12;

length = liftservopos + servowidth + 2 * servoflangelength + 5;
widthrad = servolength / 2;

rightmeat = 1.5;

difference() {
    union() {
        hull() {
            cylinder(h = thick, r = widthrad);
            translate(v = [0, length, 0]) {
                cylinder(h = thick, r = widthrad);
            }
        }
        translate(v = [-widthrad / 2, liftservopos - rightmeat, thick - fudge]) {
            difference() {
                cube(size = [servothick, servoflangelength + rightmeat, servothick]);
                translate(v = [servothick + fudge, rightmeat + servoflangelength / 2, servothick / 2]) {
                    rotate(a = [0, 180, 0]) {
                        translate(v = [0, -screwslotwidth / 2, -screwslotheight / 2]) {
                            cube(size = [screwslotdepth, screwslotwidth, screwslotheight]);
                        }
                        rotate(a = [0, 90, 0]) {
                            cylinder(h = servoscrewlen, r = servoscrewrad);
                        }
                    }
                }
                translate(v = [0, -fudge, servothick - bevel]) {
                    rotate(a = [0, -45, 0]) {
                        cube(size = [bevellen, rightmeat + servoflangelength + 2 * fudge, bevellen]);
                    }
                }
                    translate(v = [-fudge, -fudge, 4]) {
                        cube(size = [2.8 + fudge, 3 + servoflangelength + 2 * fudge, 6]);
                    }
            }
        }
        translate(v = [-widthrad / 2, liftservopos + servowidth + servoflangelength, thick - fudge]) {
            difference() {
                cube(size = [servothick, 3 + servoflangelength, servothick]);
                translate(v = [servothick + fudge, servoflangelength / 2, servothick / 2]) {
                    rotate(a = [0, 180, 0]) {
                        translate(v = [0, -screwslotwidth / 2, -screwslotheight / 2]) {
                            cube(size = [screwslotdepth, screwslotwidth, screwslotheight]);
                        }
                        rotate(a = [0, 90, 0]) {
                            cylinder(h = servoscrewlen, r = servoscrewrad);
                        }
                    }
                }
                translate(v = [0, -fudge, servothick - bevel]) {
                    rotate(a = [0, -45, 0]) {
                        cube(size = [bevellen, 3 + servoflangelength + 2 * fudge, bevellen]);
                    }
                }
            }
        }
        
        translate(v = [-widthrad / 2, elbowservopos, thick - fudge]) {
            difference() {
                cube(size = [servothick, servoflangelength, servowidth + fudge]);
                 translate(v = [servothick / 2, servoflangelength / 2, 2*fudge + servowidth - servoscrewlen]) {
                    cylinder(h = servoscrewlen, r = servoscrewrad);
                    translate(v = [screwslotheight / 2, -screwslotwidth / 2, servoscrewlen - screwslotdepth]) {
                        rotate(a = [0, 270, 0]) {
                            cube(size = [screwslotdepth, screwslotwidth, screwslotheight]);
                        }
                    }
               }
           }
        }
        translate(v = [-widthrad / 2, elbowservopos + servolength + servoflangelength, thick - fudge]) {
            difference() {
                cube(size = [servothick, servoflangelength, servowidth + fudge]);
                translate(v = [servothick / 2, servoflangelength / 2, 2*fudge + servowidth - servoscrewlen]) {
                    cylinder(h = servoscrewlen, r = servoscrewrad);
                    translate(v = [screwslotheight / 2, -screwslotwidth / 2, servoscrewlen - screwslotdepth]) {
                        rotate(a = [0, 270, 0]) {
                            cube(size = [screwslotdepth, screwslotwidth, screwslotheight]);
                        }
                    }
                }
            }
        }
        
        translate(v = [-widthrad / 2, hingepos, thick - fudge]) {
            cylinder(h = hingeheight + fudge, r = hingerad);
        }
    }
}