include <horn-169.scad>;
include <servo-169.scad>;

$fa = 3;
$fs = 1;

thick = 4.1;
fudge = 0.1;

length = 53;
widthrad = servolength / 2;

difference() {
    union() {
        hull() {
            cylinder(h = thick, r = widthrad);
            translate(v = [0, length, 0]) {
                cylinder(h = thick, r = widthrad);
            }
        }
	}

	translate(v = [0, 45, 0]) {
        rotate(a = [0, 0, 90]) {
            servo(thick, fudge);
        }
	}

    translate(v = [0, 9, thick + fudge - hthick]) {
		horn();
    }
}
